import exceptions.exceptions;
import views.views;

public class mainApp {

	public static void main(String[] args) {
		try {
			views v = new views();
			int numero = v.generarRandom();
			if(numero % 2 == 0) {
				throw new exceptions(0);
			}else {
				throw new exceptions(1);

			}
			
		}catch (exceptions e) {
			System.out.println(e.getMessage());
		}

	}

}
