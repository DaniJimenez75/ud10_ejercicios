import exceptions.exceptions;
import views.views;

public class mainApp {

	public static void main(String[] args) {
		try {
			views v = new views();
			int numero = v.generarRandom();
			if(numero % 3 == 0) {//Si el numero es divisible entre 3
				throw new exceptions(0);
			}
			else {
				throw new exceptions(1);

			}
						
		}catch (exceptions e) {
			System.out.println(e.getMessage());
		}

	}

}
