package dto;

import exceptions.exceptions;

public class operaciones {
	
	protected int valor1;
	protected int valor2;
	protected int opcion;
	
	
	public operaciones() {
		exceptions e = new exceptions();
		this.opcion = e.comprobarOpcion();
		this.valor1 = e.comprobarNumeros();

		if(opcion == 5 || opcion == 6) {//Solo se necesita un valor para hacer estas operaciones
			this.valor2 = 0;

		}else {
			this.valor2 = e.comprobarNumeros();

		}
		operacion();
	}


	public void operacion() {
		double resultado = 0;
		try {
			
			//OPERACIONES
		switch (opcion) {
		case 1:
			resultado = valor1 + valor2;
			System.out.println("La suma de los 2 valores es: "+resultado);
			break;
		case 2:
			resultado = valor1 - valor2;
			System.out.println("La resta de los 2 valores es: "+resultado);

			break;
		case 3:
			resultado = valor1 * valor2;
			System.out.println("La multiplicaci�n de los 2 valores es: "+resultado);

			break;
		case 4:
			resultado = Math.pow(valor1, valor2);
			System.out.println("La potencia es: "+resultado);

			break;
		case 5:
			resultado = Math.sqrt(valor1);
			System.out.println("La raiz cuadrada de "+valor1+" es "+resultado);


			break;
		case 6:
			resultado = Math.cbrt(valor1);
			System.out.println("La raiz c�bica de "+valor1+" es "+resultado);
			break;
		case 7:
			resultado = valor1 / valor2;
			System.out.println("La divisi�n de los 2 valores es: "+resultado);
			break;
			

		default:
			System.out.println("No existe esta opci�n");
			break;
		}
		}catch (Exception e) {//Muestra error de que no se puede dividir por 0
			System.out.println("Error en el calculo, es posible que estes diviendo por 0");
		}
	}
	
	
	


}
