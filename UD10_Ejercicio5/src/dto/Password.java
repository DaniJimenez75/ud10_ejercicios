package dto;

import java.util.Random;

public class Password {
	
	protected int longitud;
	protected String contraseña;
	
	
	public Password() {
		this.longitud = 8;
		this.contraseña = "";
		
	}


	public Password(int longitud) {
		this.longitud = longitud;
		this.contraseña = generarPassword();
		
	}
	
	
	
	
	//GETTERS
	public String getContraseña() {
		return contraseña;
	}


	public String generarPassword(){
		Random r = new Random();	
		//Array letras
		char letras[] = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
				'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
		int contador = 0;
		String password = "";
		while(contador<this.longitud) {//Mientras el contador sea mas pequeño que la longitud seguirá dentro del bucle
			
			if(contador % 2 == 0) {//Cuando el contador sea par cogera una letra del array aleatoriamente
				int l = r.nextInt(letras.length);
				char c = letras[l];
				password = password + c;
			}else {//Si no generara numeros
				int n = r.nextInt(9);
				password = password + n;
			}

			contador++;
		}
		
		return password;
	}

	public boolean esFuerte() {
		//CONTADORES
		int contadorMinusculas = 0;
		int contadorMayusculas = 0;
		int contadorNumeros = 0;
		boolean esFuerte = false;
		String[] letra = contraseña.split("");//Creamos un array de Strings separando la contraseña caracter a caracter
		
		//Vamos comprobando cada caracter si es un numero o letra mayuscula o letra minuscula
		for (int i = 0; i < letra.length; i++) {
			char caracter = letra[i].charAt(0);
			if(Character.isUpperCase(caracter)) {
				contadorMayusculas++;
				
				
			}
			if(Character.isLowerCase(caracter)) {
				contadorMinusculas++;
				
			}
			if(Character.isDigit(caracter)) {
				contadorNumeros++;
				
			}
		}
		
		//Luego comprobamos los contadores para ver si es una contraseña fuerte o no
		if(contadorMayusculas > 2 && contadorMinusculas > 1 && contadorNumeros > 5) {
			esFuerte = true;
		}
		
		return esFuerte;
	}
	

	@Override
	public String toString() {
		return "Password [longitud=" + longitud + ", contraseña=" + contraseña + "]";
	}
	
	
	
	

	
}
