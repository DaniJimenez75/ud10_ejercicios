import java.util.Scanner;

import dto.Password;
import exceptions.comprobarLongitudPassword;

public class PasswordApp {

	public static void main(String[] args) {
		
		try {
			Scanner entrada = new Scanner(System.in);	
	        System.out.print("Introduce tamaño del array: ");
	        int tamañoArray = entrada.nextInt();
	        System.out.print("Introduce longitud contraseñas: ");
	        int tamañoContraseña = entrada.nextInt();
			if(tamañoContraseña < 8) {
				throw new comprobarLongitudPassword(0); //Enviamos el codigo de error a la excepción
			}else {
				
				
				Password[] passwords = new Password[tamañoArray];//Creamos array de contraseñas
				
				//Lo llenamos de contraseñas
				for (int i = 0; i < passwords.length; i++) {
					passwords[i] = new Password(tamañoContraseña);
				}
				
				boolean[] fuertes = new boolean[tamañoArray];//Creamos array de booleans
				
				//Lo llenamos de los booleans que genere el metodo esFuerte de cada contraseña
				for (int i = 0; i < fuertes.length; i++) {
					fuertes[i] = passwords[i].esFuerte();
				}
				
				//Imprimimos los resultados
				for (int i = 0; i < fuertes.length; i++) {
					System.out.println("Contraseña: "+passwords[i].getContraseña()+" Valor: "+fuertes[i]);
				}
			}
			
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
        

		

	}
	
	
	
	
	
	

}
