package exceptions;

import views.vista;

public class exceptions {
	
	//Comprobamos que el caracter introducido sea un numero
	public int comprobarNumeroIntroducido() {
		int numeroUsuario = 0;
		try {
			vista v1 = new vista();
			numeroUsuario = v1.numeroUsuario();
			
		}catch (Exception e) {
			System.out.println("Error, el valor introducido no es un numero");
		}
			
		return numeroUsuario;
	}

}
