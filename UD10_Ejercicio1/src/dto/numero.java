package dto;

import java.util.Random;

import exceptions.exceptions;

public class numero {

	protected int contadorIntentos;
	protected int numeroAleatorio;
	protected boolean victoria;
	protected int numeroUsuario;
	
	

	public numero() {
		exceptions e = new exceptions();
		this.contadorIntentos = 0;
		this.numeroAleatorio = generarRandom();//	Guardamos numero random
		this.victoria = false;
		this.numeroUsuario = e.comprobarNumeroIntroducido(); //Guardamos numero usuario
		partida(e);//Llamamos al metodo partida
	}
	
	//GENERAMOS RANDOM
	public int generarRandom() {
		Random r = new Random();
		int numeroAleatorio = r.nextInt(500);
		return numeroAleatorio;
	}
	
	public void partida(exceptions e) {
		while(!victoria) {
			contadorIntentos++;
			if(numeroUsuario != numeroAleatorio) { //Si el numero introducido es diferente al numero random
				if(numeroUsuario < numeroAleatorio) {//Si el numero introducido es mas peque�o al numero random
					System.out.println("El numero ganador es mas grande");
				}else {
					System.out.println("El numero ganador es mas peque�o");
				}
				this.numeroUsuario = e.comprobarNumeroIntroducido();//Hacemos que vuelva a introducir otro numero

			}else {	//Si los numeros son iguales
			
				victoria = true;
				System.out.println("HAS GANADO");
				System.out.println("Numero de intentos: "+contadorIntentos);
				
			}
			
			
			
		}

	}
}
